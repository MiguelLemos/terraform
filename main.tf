
variable "resource_group_name" {}
variable "four_letter_id" {}

# Configure the Azure Provider
locals {
  subscription_id = "23683c57-86a8-4b7a-8a35-8c4770aed088"
}
provider "azurerm" {

    client_id = "fe6d69b1-a18e-46f2-ad8e-3fd988a12710"
    tenant_id = "83e4d614-6053-4858-865b-57515df7db83"
    subscription_id = "${local.subscription_id}"
}

locals{
  location = "westus2"
}
resource "azurerm_resource_group" "test" {
  name     = "${var.resource_group_name}"
  location = "${local.location}"
}

locals {
  sql_password = "FM0101t3mp#"
  sql_username = "ihadmin"
}
locals {
  flexmanage_username = "${local.flexmanage_username}"
  flexmanage_password = "Fl3xManage1!"
}
resource "azurerm_sql_server" "sql_server" {
  name                         = "fm-ih-${var.four_letter_id}-sql-server"
  resource_group_name          = "${azurerm_resource_group.test.name}"
  location = "${local.location}"
  version                      = "12.0"
  administrator_login          = "${local.sql_username}"
  administrator_login_password = "${local.sql_password}"
}

resource "azurerm_sql_firewall_rule" "test" {
  name                = "CAPITALINAS"
  resource_group_name = "${azurerm_resource_group.test.name}"
  server_name         = "${azurerm_sql_server.sql_server.name}"
  start_ip_address    = "201.216.204.0"
  end_ip_address      = "201.216.204.254"
}
resource "azurerm_sql_database" "settings" {
  name                = "settings"
  resource_group_name = "${azurerm_resource_group.test.name}"
  location = "${local.location}"
  server_name         = "${azurerm_sql_server.sql_server.name}"

  tags {
    environment = "production"
  }
}
resource "azurerm_sql_database" "db" {
  name                = "db"
  resource_group_name = "${azurerm_resource_group.test.name}"
  location = "${local.location}"
  server_name         = "${azurerm_sql_server.sql_server.name}"

  tags {
    environment = "production"
  }
}

resource "azurerm_storage_account" "common" {
  name                     = "fmih${var.four_letter_id}commonsa"
  resource_group_name      = "${azurerm_resource_group.test.name}"
  location                 = "${azurerm_resource_group.test.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
resource "azurerm_storage_account" "search" {
  name                     = "fmih${var.four_letter_id}searchsa"
  resource_group_name      = "${azurerm_resource_group.test.name}"
  location                 = "${azurerm_resource_group.test.location}"
  account_tier             = "Standard"
  account_replication_type = "LRS"
}
locals{
    docs= "documents"
}
resource "azurerm_storage_container" "documents" {
  name                  = "${local.docs}"
  resource_group_name   = "${azurerm_resource_group.test.name}"
  storage_account_name  = "${azurerm_storage_account.search.name}"
  container_access_type = "blob"
}


//az appservice plan create -n linux_webapps -g Migue  --is-linux  -l westus2      --subscription "23683c57-86a8-4b7a-8a35-8c4770aed088"

//az functionapp create --resource-group Migue --plan linux_webapps --name Migue_linux_backend_test__using_CLI --storage-account  fmihbotpib7 --runtime Node^^^|10.10

/*
locals{
  linuxServicePlan="linux-webapp-service-plan"
}
resource "null_resource" "linux_webapps_service_plan" {

  provisioner "local-exec" {
    command = "python utils/shelling/AppServicePlan.py ${local.linuxServicePlan} ${azurerm_resource_group.test.name} ${azurerm_resource_group.test.location} ${local.subscription_id} True"
  }
}*/

resource "azurerm_app_service_plan" "test" {
  name                = "azure-functions-test-service-plan"
  location            = "${azurerm_resource_group.test.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"

  sku {
    /*
    tier = "Standard"
    size = "S1"
    */

    tier = "Free"
    size = "F1"

  }
}


data "external" "example" {
  program = ["python", "utils/shelling/Bot.py",
             "fm-ih-random-bot",
             "${azurerm_resource_group.test.name}",
             "Node",
             "webapp",
             "v3",
             "description-of-my-bot",]

  query = {
    # arbitrary map from strings to strings, passed
    # to the external program as the data query.
    "KBID"= "b358b1c4-0333-4138-88e4-4f6b19328e4a"
    "Subscription_key"= "23683c57-86a8-4b7a-8a35-8c4770aed088"
    "LUIS_PRODUCTION_URL"= "https=//westus.api.cognitive.microsoft.com/luis/v2.0/apps/633d7dc1-6dcd-4edd-9f4d-d07b3db041db?verbose=true&timezoneOffset=-360&subscription-key=7572c005bc13417baba1756ebd67c235&q="
    "CL_Token"= "78a2e918f3e3bf2c846fa029538b12f5374316ed"
    "MSSQLDB"= "db"
    "MSSQLServer"= "fm-ih--demo-sql-server.database.windows.net"
    "MSSQLPwd"= "FM0101t3mp#"
    "MSSQLUserName"= "ihadmin"
  }
}
output "bot_secret" {
  value = ["${data.external.example.result}"]
}


locals{
    courtListener= "fm-ih-${var.four_letter_id}-court-listener-af"
}
resource "azurerm_function_app" "fm-ih-court-listener-af" {
  name                      = "${local.courtListener}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"
  app_settings = {}

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.courtListener} ${azurerm_resource_group.test.name} fm_lh_ms_courtlistener master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
}

locals{
    oneDrive= "fm-ih-${var.four_letter_id}-onedrive"
}
resource "azurerm_function_app" "fm_lh_ms_onedrive" {
  name                      = "${local.oneDrive}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"
  app_settings = {}

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.oneDrive} ${azurerm_resource_group.test.name} fm_lh_ms_onedrive master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
}


locals{
    oneNote= "fm-ih-${var.four_letter_id}-onenote"
}
resource "azurerm_function_app" "fm_lh_ms_onenote" {
  name                      = "${local.oneNote}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"
  app_settings = {}

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.oneNote} ${azurerm_resource_group.test.name} fm_lh_ms_onenote master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
}

locals{
    bing= "fm-ih-${var.four_letter_id}-bing"
}
resource "azurerm_function_app" "fm_lh_ms_bing" {
  name                      = "${local.bing}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"
  app_settings = {}

  provisioner "local-exec" {
    command = "python functionAppExtras.py   ${local.bing}   ${azurerm_resource_group.test.name}   fm_lh_ms_bing master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
}

locals{
    office= "fm-ih-${var.four_letter_id}-office"
}
resource "azurerm_function_app" "fm_lh_ms_office" {
  name                      = "${local.office}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"
  app_settings = {}

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.office} ${azurerm_resource_group.test.name} fm_lh_ms_office master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
}

locals{
    powerBi= "fm-ih-${var.four_letter_id}-powerbi"
}
resource "azurerm_function_app" "fm_lh_ms_powerbi" {
  name                      = "${local.powerBi}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"
  app_settings = {}

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.powerBi} ${azurerm_resource_group.test.name} fm_lh_ms_powerbi master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
}



resource "azurerm_search_service" "test" {
  name                      = "ih-cognitive-${var.four_letter_id}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  sku                       = "free"
}





locals{
    storageAf= "fm-ih-${var.four_letter_id}storage-af"
    cognitiveServiceURL = "https://${azurerm_search_service.test.name}.search.windows.net"
}
resource "azurerm_function_app" "fm-ih-storage-af" {
  name                      = "${local.storageAf}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.storageAf} ${azurerm_resource_group.test.name} fm-lh-ms-azure-storage master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
  app_settings = {


      AzureWebJobsSecretStorageType = "Blob"

      EmailAdmin = "${local.flexmanage_username}"

      EmailAdminErrorSubject = "Could not locate email to send"

      EmailBcc = ""

      EmailCc = "${local.flexmanage_username}"

      EmailHost = "smtp.office365.com"

      EmailPwd = "${local.flexmanage_password}"

      EmailSkillsOrder = "clients;matters;organizations;persons;keyPhrases|Additional Tags (Key Phrases)"

      EmailSubject = "Smart tagging complete"

      EmailUser = "${local.flexmanage_username}"

      OutlookGetEmail = "https://fm-lh-ms-outlook.azurewebsites.net/api/getemailinfo"

      OutlookUpdateStatus = "https://fm-lh-ms-outlook.azurewebsites.net/api/updatemail"

      StorageAccount = "${azurerm_storage_container.documents.name}"

      StorageAccountCnx =  "${azurerm_storage_account.search.primary_connection_string}"

      StorageApiKey = "${azurerm_search_service.test.primary_key}"

      StorageContainer = "${local.docs}"

      StorageKey = "${azurerm_storage_account.search.primary_access_key}"

      StorageSearchEndPoint = "${local.cognitiveServiceURL}/indexes/${local.docs}-index/docs?api-version=2017-11-11"

      StorageSearchKey = "${local.SearchKey}"

      StorageSearchRun = "${local.cognitiveServiceURL}/indexers/${local.docs}-indexer/run?api-version=2017-11-11-Preview"

      StorageSearchStatus = "${local.cognitiveServiceURL}/indexers/${local.docs}-indexer/status?api-version=2017-11-11-Preview"

      StorageSPPwd = "${local.flexmanage_password}"

      StorageSPSite = "https://flexmanageapps.sharepoint.com/sites/LH-DEMO/"

      StorageSPUser = "${local.flexmanage_username}"


  }
}



locals{
    searchAf= "fm-ih-${var.four_letter_id}-search-af"
    storageFunctionURL = "https://${local.storageAf}.azurewebsites.net"
    SearchKey = "25228B92ABC314CE4441E1B560505297"
}
resource "azurerm_function_app" "search_azure_function" {
  name                      = "${local.searchAf}"
  location                  = "${azurerm_resource_group.test.location}"
  resource_group_name       = "${azurerm_resource_group.test.name}"
  app_service_plan_id       = "${azurerm_app_service_plan.test.id}"
  storage_connection_string = "${azurerm_storage_account.common.primary_connection_string}"

  provisioner "local-exec" {
    command = "python functionAppExtras.py ${local.searchAf} ${azurerm_resource_group.test.name} fm_lh_ms_azure_search master"
  }
  lifecycle {
      ignore_changes = [
         "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
         "version",
         "app_settings.%"
     ]
  }
  app_settings = {


    APIKey = "${azurerm_search_service.test.primary_key}"

    AZ_Host = "https://${local.searchAf}.azurewebsites.net"

    AZPipe_ApiKey = "${azurerm_search_service.test.primary_key}"

    AZPipe_DataSourceName = "${local.docs}"

    AZPipe_IndexerName = "${local.docs}-indexer"

    AZPipe_IndexName = "${local.docs}-index"

    AZPipe_ServiceEndpoint = "${local.cognitiveServiceURL}"

    AZPipe_SkillsetName = "lh-cognitive-skillset"

    AZPipe_StorageAccount = "${azurerm_storage_container.documents.name}"

    AZPipe_StorageCnxString =  "${azurerm_storage_account.search.primary_connection_string}"

    AZPipe_StorageContainer = "${local.docs}"

    AZStorageFileStatus = "${local.storageFunctionURL}/api/file_status"

    AZStorageUpdateMetadata = "${local.storageFunctionURL}/api/generate_update_metadata_key"

    AzureSearchEndpoint = "${local.cognitiveServiceURL}"

    AzureWebJobsSecretStorageType = "Blob"

    ClientData = "tranClientIntake"

    ClientMatchMinThreshold = "0.5"



    MatterData = "tranMatterIntake"

    MatterMatchMinThreshold = "0.5"

    MSSQL_DB = "${azurerm_sql_database.db.name}"

    MSSQL_Host = "${azurerm_sql_server.sql_server.name}.database.windows.net"

    MSSQL_Password = "${local.sql_password}"

    MSSQL_User = "${local.sql_username}"

    SearchKey = "${local.SearchKey}"


  }
}

locals {
  backendRepository = "ih-backend"
  backendBranch = "master"
  backendWebApp = "fm-ih-${var.four_letter_id}--backend"
}
resource "azurerm_app_service" "backendWebApp" {
  name                = "${local.backendWebApp}"
  location            = "${azurerm_resource_group.test.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
  app_service_plan_id = "${azurerm_app_service_plan.test.id}"

  app_settings {
    MSSQLDB = "${azurerm_sql_database.settings.name}"
    MSSQLPwd = "${local.sql_password}"
    MSSQLServer =  "${azurerm_sql_server.sql_server.name}.database.windows.net"
    MSSQLUserName = "${local.sql_username}"
    "WEBSITE_NODE_DEFAULT_VERSION" = "10.6.0"
  }

  provisioner "local-exec" {
    command = "python utils/shelling/WebApp.py ${local.backendWebApp} ${azurerm_resource_group.test.name} ${local.backendRepository}  ${local.backendBranch} ${azurerm_app_service_plan.test.id} ${local.subscription_id}"
  }

}


locals {
  adminRepository = "ih-web"
  adminBranch = "master"
  adminWebappName = "fm-ih-${var.four_letter_id}--admin"
}
resource "azurerm_app_service" "adminWebappName" {
  name                = "${local.adminWebappName}"
  location            = "${azurerm_resource_group.test.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
  app_service_plan_id = "${azurerm_app_service_plan.test.id}"

  app_settings {
    MSSQLDB = "${azurerm_sql_database.settings.name}"
    MSSQLPwd = "${local.sql_password}"
    MSSQLServer =  "${azurerm_sql_server.sql_server.name}.database.windows.net"
    MSSQLUserName = "${local.sql_username}"
    "WEBSITE_NODE_DEFAULT_VERSION" = "10.6.0"
  }

  provisioner "local-exec" {
    command = "python utils/shelling/WebApp.py ${local.adminWebappName} ${azurerm_resource_group.test.name} ${local.adminRepository}  ${local.adminBranch} ${azurerm_app_service_plan.test.id} ${local.subscription_id}"
  }
}

locals {
  frontendRepository = "fm-react-layout-sp"
  frontendBranch = "develop"
  frontendWebApp = "fm-ih-${var.four_letter_id}--react"
}
resource "azurerm_app_service" "frontendWebApp" {
  name                = "${local.frontendWebApp}"
  location            = "${azurerm_resource_group.test.location}"
  resource_group_name = "${azurerm_resource_group.test.name}"
  app_service_plan_id = "${azurerm_app_service_plan.test.id}"

  app_settings {

    "NODE_PATH" =  "src",
    "REACT_APP_SHAREPOINT_URL" =  "https = //fm-lh-ms-sharepoint.azurewebsites.net/api/search?code=axQ1f5HYsyYETwnjB7eBNYX04K4zFBq27MadlAcsQKkJyiLDJdFQPQ==",
    "REACT_APP_AZURESEARCH_URL" =  "https = //fm-ih-ms-azure-search.azurewebsites.net/api/search_index",
    "REACT_APP_LAYOUT_URL" =  "https = //fm-ih-backend-test.azurewebsites.net/layout",
    "REACT_APP_API_URL" =  "https = //fm-ih-backend-test.azurewebsites.net/api"
    "REACT_APP_BOT_DIRECTLINE" = "${data.external.example.result["bot_secret"]}"
    "WEBSITE_NODE_DEFAULT_VERSION" = "10.6.0"

  }
  provisioner "local-exec" {
    command = "python utils/shelling/WebApp.py ${local.frontendWebApp} ${azurerm_resource_group.test.name} ${local.frontendRepository}  ${local.frontendBranch} ${azurerm_app_service_plan.test.id} ${local.subscription_id}"
  }
}
