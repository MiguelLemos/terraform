import os
import subprocess
import shutil
import yaml
import argparse
from utils.shelling import WebApp
from utils.string import AzureFunction
import hcl
import json

controller = {
    "provider" : AzureFunction.create,
    "function_app" : AzureFunction.create
}

def walk(node, name=""):
    items = node.items()
    walkable_items = {(k, v) for k, v in items if isinstance(v, dict)}
    not_walkable_items = dict(filter(lambda x: not isinstance(x,dict), items))

    for key, item in walkable_items:
        walk(item, key)

    config = {name:not_walkable_items}
    print(config)
    print()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    # parser.add_argument('NTimes', type=int)
    args = parser.parse_args()

    # Read YAML file
    with open("config.yaml", 'r') as stream:
        data_loaded = yaml.load(stream)
        hierarchy = data_loaded["hierarchy"]

        walk(data_loaded["plan"])



# crear subscripcion con un budget
