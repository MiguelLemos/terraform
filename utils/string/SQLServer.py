
def create(name="name",
           location="location",
           reference_id="reference_id",
           version="version",
           administrator_login="administrator_login",
           administrator_login_password="administrator_login_password"):
    return '''
    resource "azurerm_sql_server" "''' + reference_id + '''" {

      name     = "''' + name + '''"
      location = "''' + location + '''"
      version                      = "''' + version + '''"
      administrator_login          = "''' + administrator_login + '''"
      administrator_login_password = "''' + administrator_login_password + '''"
    }
    '''

def main(**kwargs):
    settings = [key + " = " + value for key, value in kwargs.items()]
    s = "\n\t"
    return f'''
    resource "${kwargs["resource"]}" "${kwargs["terraform_name"]}" {{
     ''' + s + s.join(settings) + '''
    }
    '''

def inc(x):
    return x + 1


def test_answer():
    assert inc(3) == 5


if __name__ == "__main__":
    result = main(
        resource= "azurerm_resource_group",
        terraform_name= "terraform_name",
        location= "location"
    )
    print(result)
