import json


def to_singleline(multilineString):  return " ".join(multilineString.replace('\n', ' ').split()).strip()
