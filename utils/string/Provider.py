
def create(client_id="client_id",
           tenant_id="tenant_id",
           subscription_id="subscription_id",
           provider="azurerm"):
    return '''
    provider "''' + provider + '''" {

        client_id = "''' + client_id + '''"
        tenant_id = "''' + tenant_id + '''"
        subscription_id = "''' + subscription_id + '''"
    }
    '''
