
from utils.string.generic.Resource import create
from utils.string.HCL.utils import reference

def create_function_app(name, service_plan, storage_account, resource_group, repository, branch):
    # TODO Model HCL-Terraform hierarchy in OOP
    resource_group_name =   reference("azurerm_resource_group",   resource_group,  "name")
    location =              reference("azurerm_resource_group",   resource_group,  "location")
    app_service_plan_id =   reference("azurerm_app_service_plan", service_plan,    "id")
    storage_account =       reference("azurerm_storage_account",  storage_account, "primary_connection_string")

    result = create(
        resource = "azurerm_function_app",
        resource_group_name = resource_group_name,
        name = name,
        location= location,
        storage_account = storage_account,
        extra='''
        app_settings = {}

        provisioner "local-exec" {
          command = "python functionAppExtras.py ''' + " ".join([name, resource_group, repository, branch]) + '''
        }
        lifecycle {
            ignore_changes = [
               "app_settings.WEBSITE_NODE_DEFAULT_VERSION",
               "version",
               "app_settings.%"
           ]
        }

        '''
    )
    return result


if __name__ == "__main__":

    name="name"
    service_plan="service_plan"
    storage_account="storage_account"
    resource_group="resource_group"
    repository="repository"
    branch="branch"
    location="location"

    result = create_function_app(name, service_plan, storage_account, resource_group, repository, branch)
    print(result)
