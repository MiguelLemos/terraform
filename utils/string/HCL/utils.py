
def reference(resource, terraform_name, attribute):
    return '"${' + resource + '.' + terraform_name + '.' + attribute + '}"'



if __name__ == "__main__":
    result = reference(
        resource= "azurerm_resource_group",
        terraform_name= "terraform_name",
        attribute= "attribute"
    )
    print(result)
