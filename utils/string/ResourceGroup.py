
def create(name="name",
           location="location"):
    return '''
    resource "azurerm_resource_group" "''' + name + '''" {

      name     = "''' + name + '''"
      location = "''' + location + '''"
      
    }
    '''

if __name__ == "__main__":

    name="name"
    location="location"

    result = create(name, location)
    print(result)
