
from utils.string.SQLServer import main
from utils.string.utils.string import to_singleline
def to_singleline(multilineString):  return " ".join(multilineString.replace('\n', ' ').split()).strip()


golden_rule = to_singleline("""

resource "$azurerm_resource_group" "$terraform_name" {

    resource = azurerm_resource_group
    terraform_name = terraform_name
    location = location
}

"""

def test_SQLServer_template():
    assert golden_rule == to_singleline(main(
        resource= "azurerm_resource_group",
        terraform_name= "terraform_name",
        location= "location"))
