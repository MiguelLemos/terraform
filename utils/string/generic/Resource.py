
def create(extra = "", **kwargs):
    settings = [key + " = " + value for key, value in kwargs.items()]
    s = "\n\t"
    return f'''
    resource "{kwargs["resource"]}" "{kwargs["name"]}" {{
     ''' + s + s.join(settings) + '''

     ''' + s + extra + '''
    }
    '''



if __name__ == "__main__":
    result = create(
        resource= "azurerm_resource_group",
        name = "name",
        location= "location"
    )
    print(result)
