#!/usr/bin/python
import os
import sys
import shutil, stat



import utils

from utils.string import json_to_multiline_string
from utils.system import executeToString



def cloneAndBuild(
    username,
    password,
    repository_name,
    branch,
    env_variables={
      'NODE_PATH':'src'
    , 'REACT_APP_SHAREPOINT_URL':  'https://fm-lh-ms-sharepoint.azurewebsites.net/api/search?code=axQ1f5HYsyYETwnjB7eBNYX04K4zFBq27MadlAcsQKkJyiLDJdFQPQ=='
    , 'REACT_APP_AZURESEARCH_URL':  'https://fm-ih-ms-azure-search.azurewebsites.net/api/search_index'
    , 'REACT_APP_LAYOUT_URL': 'https://fm-ih-backend-test.azurewebsites.net/layout'
    , 'REACT_APP_API_URL': 'https://fm-ih-backend-test.azurewebsites.net/api'}):
    git_clone = executeToString(f"git clone -b {branch} https://{username}:{password}@bitbucket.org/legalhivedevteam/{repository_name}")
    old_dir = os.getcwd()
    os.chdir(repository_name)
    npm_install = executeToString(f"npm install" )  #executeToString(f"npm install" )
    CUSTOM_ENV_CONTENT =  json_to_multiline_string(env_variables)
    CUSTOM_ENV_FILENAME = ".env.CUSTOM"
    with open(CUSTOM_ENV_FILENAME, "w") as f: f.write(CUSTOM_ENV_CONTENT)
    with open("package.json", "r") as f:
        content = f.read()
        envcmd = "env-cmd" in content
        react =  "react"   in content
    build = {}
    if envcmd and react:
        build = executeToString(f"yarn env-cmd .env.CUSTOM  react-scripts build" )
    os.chdir(old_dir)
    return {"git_clone": git_clone, "npm_install": npm_install, "build": build}


def deployCode(resource_group, repository_name, name):
    output_filename = repository_name
    zippedPath = f"{repository_name}/{output_filename}"
    zippedPathWithExtension = zippedPath + ".zip"
    try:
        shutil.make_archive(zippedPath, 'zip', f"{repository_name}/build")
    except:
        shutil.make_archive(zippedPath, 'zip', f"{repository_name}")
    deploy_webapp =  executeToString(f"az webapp deployment source config-zip -g {resource_group} -n {name}  --src {zippedPathWithExtension}")
    return {"deploy_webapp": deploy_webapp}

def createEmptyWebApp(name="name",
           plan="plan",
           resource_group="resource_group",
           subscription="subscription",
           runtime="node|10.6"):
    create_web_app_multiline_with_spaces = f"""az webapp create
     --name {name}
     --plan {plan}
     --resource-group {resource_group}
     --runtime {runtime.replace("|", "^^^|")}
     --subscription {subscription}
    """
    create_web_app_with_spaces = create_web_app_multiline_with_spaces.replace('\n', ' ')
    create_web_app = executeToString(" ".join(create_web_app_with_spaces.split()))
    return {"create_web_app": create_web_app}

def cleanup(dir_name):
    def on_rm_error( func, path, exc_info):
        # path contains the path of the file that couldn't be removed
        # let's just assume that it's read-only and unlink it.
        os.chmod( path, stat.S_IWRITE )
        os.unlink( path )
    shutil.rmtree( dir_name, onerror = on_rm_error )

def main(name, resource_group, repository_name, branch, app_service_plan_id, subscription_id):
    username = "mlemos%40overactive.com"
    password = "osopanda"

    results = [
      cloneAndBuild(username, password, repository_name, branch)
    , createEmptyWebApp(name=name, plan=app_service_plan_id, subscription=subscription_id, resource_group=resource_group)
    , deployCode(resource_group, repository_name, name)
    ]
    cleanup(f"./{repository_name}")
    return results



"""
python utils/shelling/WebApp.py fm-ih-main--admin Migue ih-web  master /subscriptions/23683c57-86a8-4b7a-8a35-8c4770aed088/resourceGroups/Migue/providers/Microsoft.Web/serverfarms/azure-functions-test-service-plan 23683c57-86a8-4b7a-8a35-8c4770aed088
"""
if __name__ == "__main__":
    name= sys.argv[1]
    resource_group= sys.argv[2]
    repository_name= sys.argv[3]
    branch= sys.argv[4]
    app_service_plan_id= sys.argv[5]
    subscription_id= sys.argv[6]
    main(name, resource_group, repository_name, branch, app_service_plan_id, subscription_id)
