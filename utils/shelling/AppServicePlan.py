#!/usr/bin/python
import sys


from utils.system import execute

def main(name="linux_webapps", resource_group="Migue", location="westus2", subscription="23683c57-86a8-4b7a-8a35-8c4770aed088", linux=True):
    result = execute(f"""
    az appservice plan create
        --name {name}
        --resource-group {resource_group}
        --location {location}
        {"--is-linux" if linux else ""}
        --subscription {subscription}
    """, stringify_result=True)
    print(result)


if __name__=='__main__':
    if (len(sys.argv) < 4):
        main()
    else:
        name= sys.argv[1]
        resource_group= sys.argv[2]
        location= sys.argv[3]
        subscription= sys.argv[4]
        linux= sys.argv[5]
        main(name, resource_group, location, subscription, linux)
