import os
import sys
import json
import uuid

try:
    from utils.string import json_to_multiline_string
    from utils.system import silentlyExecuteToString, silentlyExecuteToJSON
except ImportError:
    from utils.shelling.utils.string import json_to_multiline_string
    from utils.shelling.utils.system import silentlyExecuteToString, silentlyExecuteToJSON


def main(  name=str(uuid.uuid4())[0:23],
           resource_group="Migue",
           lang="Node",
           kind="webapp",
           version="v3",
           description="description-of-my-bot",
           app_settings={
            "KBID": "b358b1c4-0333-4138-88e4-4f6b19328e4a"
           ,"Subscription_key": "23683c57-86a8-4b7a-8a35-8c4770aed088"
           ,"LUIS_PRODUCTION_URL": "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/633d7dc1-6dcd-4edd-9f4d-d07b3db041db?verbose=true&timezoneOffset=-360&subscription-key=7572c005bc13417baba1756ebd67c235&q="
           ,"CL_Token": "78a2e918f3e3bf2c846fa029538b12f5374316ed"
           ,"MSSQLDB": "db"
           ,"MSSQLServer": "fm-ih--demo-sql-server.database.windows.net"
           ,"MSSQLPwd": "FM0101t3mp#"
           ,"MSSQLUserName": "ihadmin"}):

    default_password = "FM0101t3mp#"
    create_app = f"""az ad app create
         --display-name {name}
         --password {default_password}
         --identifier-uris www.{name}.com/{str(uuid.uuid4())} """
    MICROSOFT_APP_PASSWORD = default_password
    MICROSOFT_APP_ID =  silentlyExecuteToJSON(create_app)["appId"]

    create_web_app = \
    silentlyExecuteToJSON(f"""az bot create
        --resource-group {resource_group}
        --name {name}
        --kind {kind}
        --version {version}
        --description {description}
        --lang {lang}
        --appid {MICROSOFT_APP_ID}
        --password {MICROSOFT_APP_PASSWORD}
    """)
    WEBAPP_NAME = create_web_app["endpoint"][len("https://"):-len(".azurewebsites.net/api/messages")]


    return {
    "set_bot_direct_line": silentlyExecuteToString(f"""az bot directline create -n {name} -g {resource_group}"""),
    "bot_secret": silentlyExecuteToJSON(f"az bot directline show --with-secrets -n {name} -g {resource_group}")['properties']['properties']['sites'][0]['key'],
    "set_deploy_repository": silentlyExecuteToString(f"""az webapp deployment source config
        -g  {resource_group} -n {WEBAPP_NAME}
        --repo-url "https://bitbucket.org/legalhivedevteam/legalhive-bot"
        --branch "diego-bot"
        --private-repo-username "mlemos%40overactive.com"
        --private-repo-password "osopanda"

    """),
    "set_webapp_settings": silentlyExecuteToString( f"""az webapp config appsettings set
        --resource-group {resource_group} -n {WEBAPP_NAME}
        --settings

            MICROSOFT_APP_ID = {MICROSOFT_APP_ID}
            MICROSOFT_APP_PASSWORD = {MICROSOFT_APP_PASSWORD}

            ENCRYPT = true
            {json_to_multiline_string(app_settings)}
    """)
    }




if __name__=='__main__':
    if (len(sys.argv) < 5):
        result = main()
    else:
        result = main(sys.argv[1], # name
             sys.argv[2], # resource_group
             sys.argv[3], # lang
             sys.argv[4], # kind
             sys.argv[5], # version
             sys.argv[6], # description
             **dict((setting.split("=")[0], "=".join(setting.split("=")[1:]))
             for setting in sys.argv[7:])) # kwargs
    print(json.dumps({"bot_secret": result["bot_secret"]}))
