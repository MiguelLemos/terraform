
from utils.shelling.WebApp import main
from utils.string.utils.string import to_singleline
from utils.shelling.utils.system import execute

args = {
      "name": 'fm-ih-main--backend'
    , "resource_group": 'Migue'
    , "repository_name": 'ih-backend'
    , "branch": 'master'
    , "app_service_plan_id": '/subscriptions/23683c57-86a8-4b7a-8a35-8c4770aed088/resourceGroups/Migue/providers/Microsoft.Web/serverfarms/azure-functions-test-service-plan'
    , "subscription_id": '23683c57-86a8-4b7a-8a35-8c4770aed088'
}
def test_SQLServer_template():
    test =  to_singleline("""

    resource "$azurerm_resource_group" "$terraform_name" {

        resource = azurerm_resource_group
        terraform_name = terraform_name
        location = location
    }

    """)
    result = main(**args)
    print()
    print(result)
    print("already exists" in result)
    assert test == to_singleline(result)

'''
 execute(f""" python ../WebApp.py
                ${args["name"]}
                ${args["resource_group"]}
                ${args["branch"]}
                ${args["app_service_plan_id"]}
                ${args["subscription_id"]}""")
'''
