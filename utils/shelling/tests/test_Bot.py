
import uuid
import unittest
from utils.shelling.Bot import main
from utils.string.utils.string import to_singleline
from utils.shelling.utils.system import executeToString


app_settings = {
 "KBID": "b358b1c4-0333-4138-88e4-4f6b19328e4a"
,"Subscription_key": "23683c57-86a8-4b7a-8a35-8c4770aed088"
,"LUIS_PRODUCTION_URL": "https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/633d7dc1-6dcd-4edd-9f4d-d07b3db041db?verbose=true&timezoneOffset=-360&subscription-key=7572c005bc13417baba1756ebd67c235&q="
,"CL_Token": "78a2e918f3e3bf2c846fa029538b12f5374316ed"
,"MSSQLDB": "db"
,"MSSQLServer": "fm-ih--demo-sql-server.database.windows.net"
,"MSSQLPwd": "FM0101t3mp#"
,"MSSQLUserName": "ihadmin"}

class MyTestCase(unittest.TestCase):
    def test_default_greeting_set(self):
        result = main(
                    name=str(uuid.uuid4())[0:23],
                   resource_group="Migue",
                   lang="Node",
                   kind="webapp",
                   version="v3",
                   description="description-of-my-bot",
                   app_settings=app_settings
                   )
        print(result)
        for key, value in result.items():
            print(key, value)
        self.assertEqual( "MSSQLUserName" in result["set_webapp_settings"],True)
        return result

if __name__ == '__main__':
    unittest.main()
