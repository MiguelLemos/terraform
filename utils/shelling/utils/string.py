import json

def json_to_multiline_string(object):
    def no_braces(string): return string[1:-1]
    def double_point_to_equal(string): return string.replace(":"," = ")
    def object_to_string(object): return json.dumps(object, indent=0)
    return no_braces(double_point_to_equal(object_to_string(object)))

def to_singleline(multilineString):  return " ".join(multilineString.replace('\n', ' ').split()).strip()
def replacements(string):  return string.replace(" = ", "=")
def clean(multilineString): return replacements(to_singleline(multilineString))
