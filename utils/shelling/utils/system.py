import os
from subprocess import STDOUT, check_output
import json
from colorama import Fore, Back, Style
from colorama import init
init(convert=True)

try:
    from utils.string import clean, json_to_multiline_string
except:
    from utils.shelling.utils.string import clean, json_to_multiline_string
def execute(multilineString, show=True):
    if show: _show(multilineString)
    cmd = clean(multilineString)
    result = check_output(cmd, shell=True, stderr=STDOUT, timeout=60*60).decode("utf-8")
    return result

def _show(multilineString):
    print()
    print(Back.GREEN + Fore.WHITE + multilineString + Style.RESET_ALL)
    print(clean(multilineString))
    print()

def executeToString(multilineString):
    return Back.WHITE + Fore.BLACK + execute(multilineString, show=True) + Style.RESET_ALL
def silentlyExecuteToString(multilineString):
    return Back.WHITE + Fore.BLACK + execute(multilineString, show=False) + Style.RESET_ALL

def executeToJSON(multilineString):
    return json.loads(execute(multilineString, show=True))
def silentlyExecuteToJSON(multilineString):
    return json.loads(execute(multilineString, show=False))
