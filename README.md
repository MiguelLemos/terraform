
#### An opinionated wrapper on top of Terraform Azure provider

-------


# installation

You will need you install Azure CLI, Terraform and Python

## For Windows
https://aka.ms/installazurecliwindows

https://releases.hashicorp.com/terraform/0.11.11/terraform_0.11.11_windows_386.zip

https://www.python.org/downloads/release/python-372/

# how to run
terraform plan

	This will provide of an overview of the changes needed to achieve the desired end state described by the .tf document.

terraform apply

	This will ask for your approval and then will procede to apply the needed changes to the infrastructure.

# how to ask for instructions
//TODO python main.py help


  
  
  
  
# short explanation
We are using Terraform to plan our infrastructure, and execute said plans.

Terraform allows us to know what are the necessary changes to transform the current state of the infrastructure to the desired one.

With ``` terraform plan ``` we get that piece of information.

With ``` terraform apply ``` we tell Terraform to apply the neccesary changes. 

# long explanation

https://www.terraform.io/intro/index.html
##### What is Terraform?
Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.

Configuration files describe to Terraform the components needed to run a single application or your entire datacenter. Terraform generates an execution plan describing what it will do to reach the desired state, and then executes it to build the described infrastructure. As the configuration changes, Terraform is able to determine what changed and create incremental execution plans which can be applied.

The infrastructure Terraform can manage includes low-level components such as compute instances, storage, and networking, as well as high-level components such as DNS entries, SaaS features, etc.

Examples work best to showcase Terraform. Please see the use cases.

The key features of Terraform are:

##### Infrastructure as Code
Infrastructure is described using a high-level configuration syntax. This allows a blueprint of your datacenter to be versioned and treated as you would any other code. Additionally, infrastructure can be shared and re-used.

##### Execution Plans
Terraform has a "planning" step where it generates an execution plan. The execution plan shows what Terraform will do when you call apply. This lets you avoid any surprises when Terraform manipulates infrastructure.

##### Resource Graph
Terraform builds a graph of all your resources, and parallelizes the creation and modification of any non-dependent resources. Because of this, Terraform builds infrastructure as efficiently as possible, and operators get insight into dependencies in their infrastructure.

##### Change Automation
Complex changesets can be applied to your infrastructure with minimal human interaction. With the previously mentioned execution plan and resource graph, you know exactly what Terraform will change and in what order, avoiding many possible human errors.